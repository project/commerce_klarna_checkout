<?php

namespace Drupal\Tests\commerce_klarna_checkout\Kernel;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Tests the request builder.
 *
 * @coversDefaultClass \Drupal\commerce_klarna_checkout\RequestBuilder
 *
 * @group commerce_klarna_checkout
 */
class KlarnaRequestBuilderTest extends OrderKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_promotion',
    'commerce_klarna_checkout',
  ];

  /**
   * The request builder.
   *
   * @var \Drupal\commerce_klarna_checkout\RequestBuilder
   */
  protected $requestBuilder;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('commerce_promotion');
    $this->installEntitySchema('commerce_promotion_coupon');
    $this->installConfig(['commerce_promotion']);
    $this->installSchema('commerce_promotion', ['commerce_promotion_usage']);

    $tax_adjustment = new Adjustment([
      'type' => 'tax',
      'label' => 'VAT',
      'amount' => new Price('2', 'USD'),
      'source_id' => 'us_vat|default|standard',
      'percentage' => '0.12',
      'included' => TRUE,
      'locked' => TRUE,
    ]);
    $promotion_adjustment = new Adjustment([
      'type' => 'promotion',
      'label' => 'Discount',
      'amount' => new Price('-5.00', 'USD'),
      'locked' => TRUE,
      'source_id' => '',
    ]);
    $promotion_adjustment_included = new Adjustment([
      'type' => 'promotion',
      'label' => 'Discount',
      'amount' => new Price('-5.00', 'USD'),
      'locked' => TRUE,
      'source_id' => '',
      'included' => TRUE,
    ]);
    $custom_adjustment = new Adjustment([
      'type' => 'custom',
      'label' => '10% off',
      'amount' => new Price('-1.00', 'USD'),
      'percentage' => '0.1',
      'locked' => TRUE,
    ]);
    $order_item = OrderItem::create([
      'type' => 'test',
    ]);
    $order_item->setTitle('My order item');
    $order_item->setQuantity('2');
    $unit_price = new Price('10.00', 'USD');
    $order_item->setUnitPrice($unit_price, TRUE);
    $order_item->addAdjustment($tax_adjustment);
    $order_item->addAdjustment($promotion_adjustment);
    $order_item->addAdjustment($promotion_adjustment_included);
    $order_item->addAdjustment($custom_adjustment);
    $order_item->save();

    $this->order = Order::create([
      'type' => 'default',
      'store_id' => $this->store,
    ]);
    $this->order->addItem($order_item);
    $this->order->save();

    $this->requestBuilder = $this->container->get('commerce_klarna_checkout.request_builder');
  }

  /**
   * @covers ::buildOrderLines
   */
  public function testAdjustments() {
    $orderLines = $this->requestBuilder->buildOrderLines($this->order);
    $this->assertEquals([
      [
        'reference' => '1',
        'name' => 'My order item',
        'quantity' => 2,
        'tax_rate' => 1200,
        'total_tax_amount' => 200,
        'unit_price' => 1000,
        'total_amount' => 1500,
        'total_discount_amount' => 500,
      ],
      [
        'reference' => '',
        'name' => '10% off',
        'quantity' => 1,
        'tax_rate' => 0,
        'total_tax_amount' => 0,
        'unit_price' => -100,
        'total_amount' => -100,

      ],
    ], $orderLines);
  }

}
