<?php

namespace Drupal\Tests\commerce_klarna_checkout\Unit;

use CommerceGuys\Addressing\Address;
use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\commerce_klarna_checkout\RequestBuilder;
use Drupal\commerce_order\AdjustmentTransformerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Entity\CurrencyInterface;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the request builder.
 *
 * @coversDefaultClass \Drupal\commerce_klarna_checkout\RequestBuilder
 *
 * @group commerce_klarna_checkout
 */
class KlarnaRequestBuilderTest extends UnitTestCase {

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The Klarna request builder.
   *
   * @var \Drupal\commerce_klarna_checkout\RequestBuilder
   */
  protected $klarnaRequestBuilder;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $first_order_item = $this->prophesize(OrderItemInterface::class);
    $first_order_item->id()->willReturn(1);
    $first_order_item->getPurchasedEntity()->willReturn(NULL);
    $first_order_item->label()->willReturn('Order item 1');
    $first_order_item->getEntityTypeId()->willReturn('commerce_order_item');
    $first_order_item->getQuantity()->willReturn('2000');
    $first_order_item->getUnitPrice()->willReturn(new Price('12.34', 'NOK'));
    $first_order_item->getAdjustedTotalPrice(['promotion'])->willReturn(new Price('12.34', 'NOK'));
    $first_order_item->getAdjustments(['tax'])->willReturn([]);
    $first_order_item->getAdjustments(['promotion'])->willReturn([]);
    $first_order_item = $first_order_item->reveal();

    $second_order_item = $this->prophesize(OrderItemInterface::class);
    $second_order_item->id()->willReturn(2);
    $second_order_item->getPurchasedEntity()->willReturn(NULL);
    $second_order_item->label()->willReturn('Order item 2');
    $second_order_item->getEntityTypeId()->willReturn('commerce_order_item');
    $second_order_item->getQuantity()->willReturn('1000');
    $second_order_item->getUnitPrice()->willReturn(new Price('10.00', 'NOK'));
    $second_order_item->getAdjustedTotalPrice(['promotion'])->willReturn(new Price('10.00', 'NOK'));
    $second_order_item->getAdjustments(['tax'])->willReturn([]);
    $second_order_item->getAdjustments(['promotion'])->willReturn([]);
    $second_order_item = $second_order_item->reveal();

    $address_list = $this->prophesize(FieldItemListInterface::class);
    $address_list->first()->willReturn(new Address('NO', 'Trøndelag', 'Trondheim', '', '7042', '', 'Storgata 1', '', '', 'Bedriften AS', 'Per', '', 'Post'));
    $address_list = $address_list->reveal();
    $profile = $this->prophesize(ProfileInterface::class);
    $profile->get('address')->willReturn($address_list);
    $profile = $profile->reveal();

    $billing_countries_list = $this->prophesize(FieldItemListInterface::class);
    $billing_countries_list->getValue()->willReturn([
      [
        'value' => 'NO',
      ],
      [
        'value' => 'IL',
      ],
    ]);
    $billing_countries_list = $billing_countries_list->reveal();
    $shipping_countries_list = $this->prophesize(FieldItemListInterface::class);
    $shipping_countries_list->getValue()->willReturn([]);
    $shipping_countries_list = $shipping_countries_list->reveal();
    $store = $this->prophesize(StoreInterface::class);
    $store->label()->willReturn('Store name');
    $store->get('billing_countries')->willReturn($billing_countries_list);
    $store->hasField('shipping_countries')->willReturn(TRUE);
    $store->get('shipping_countries')->willReturn($shipping_countries_list);
    $store = $store->reveal();

    $order = $this->prophesize(OrderInterface::class);
    $order->getEmail()->willReturn('test@example.com');
    $order->getEntityTypeId()->willReturn('commerce_order');
    $order->getItems()->willReturn([$first_order_item, $second_order_item]);
    $order->getBillingProfile()->willReturn($profile);
    $order->collectAdjustments()->willReturn([]);
    $order->hasField('shipments')->willReturn(FALSE);
    $order->getTotalPrice()->willReturn(new Price('22.34', 'NOK'));
    $order->getStore()->willReturn($store);
    $order->getOrderNumber()->willReturn('order number');
    $order->id()->willReturn(1);
    $order->collectProfiles()->willReturn([$profile]);
    $this->order = $order->reveal();

    $nok_currency = $this->prophesize(CurrencyInterface::class);
    $nok_currency->id()->willReturn('NOK');
    $nok_currency->getFractionDigits()->willReturn('2');

    $storage = $this->prophesize(EntityStorageInterface::class);
    $storage->load('NOK')->willReturn($nok_currency->reveal());

    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $entity_type_manager->getStorage('commerce_currency')->willReturn($storage->reveal());
    $entity_type_manager = $entity_type_manager->reveal();

    $country_repository = $this->prophesize(CountryRepositoryInterface::class);
    $country_repository->getList()->willReturn([
      'FR' => 'France',
      'PL' => 'Poland',
      'NO' => 'Norway',
    ]);

    $minor_units_converter = $this->createMock(MinorUnitsConverterInterface::class);
    $minor_units_converter->expects($this->any())
      ->method('toMinorUnits')
      ->will($this->returnCallback(function (Price $amount) {
        $number = $amount->getNumber();
        $number = Calculator::multiply($number, pow(10, 2));
        return (int) round($number);
      }));

    $this->klarnaRequestBuilder = new RequestBuilder($this->prophesize(AdjustmentTransformerInterface::class)->reveal(), $country_repository->reveal(), $entity_type_manager, $minor_units_converter);
  }

  /**
   * @covers ::buildAddress
   */
  public function testBuildAddress() {
    $address = $this->klarnaRequestBuilder->buildAddress($this->order->getBillingProfile());
    $this->assertEquals([
      'city' => 'Trondheim',
      'country' => 'NO',
      'family_name' => 'Post',
      'given_name' => 'Per',
      'organization_name' => 'Bedriften AS',
      'postal_code' => '7042',
      'region' => 'Trøndelag',
      'street_address' => 'Storgata 1',
      'street_address2' => '',
    ], $address);
  }

  /**
   * @covers ::buildOrderLines
   */
  public function testOrderLines() {
    $orderLines = $this->klarnaRequestBuilder->buildOrderLines($this->order);
    $this->assertEquals([
      0 => [
        'reference' => '1',
        'name' => 'Order item 1',
        'quantity' => 2000,
        'unit_price' => 1234,
        'total_amount' => 1234,
        'tax_rate' => 0,
        'total_tax_amount' => 0,
      ],
      1 => [
        'reference' => '2',
        'name' => 'Order item 2',
        'quantity' => 1000,
        'unit_price' => 1000,
        'total_amount' => 1000,
        'tax_rate' => 0,
        'total_tax_amount' => 0,
      ],
    ], $orderLines);
  }

  /**
   * @covers ::buildOrder
   */
  public function testOrder() {
    $order = $this->klarnaRequestBuilder->buildOrder($this->order);
    $this->assertEquals('NOK', $order['purchase_currency']);
    $this->assertEquals('Store name', $order['name']);
    $this->assertEquals(2234, $order['order_amount']);
    $this->assertEquals('order number', $order['merchant_reference1']);
    $this->assertEquals(1, $order['merchant_reference2']);
    $this->assertEquals(['NO', 'IL'], $order['billing_countries']);
    $this->assertEquals(['FR', 'PL', 'NO'], $order['shipping_countries']);
  }

}
