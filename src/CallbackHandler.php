<?php

namespace Drupal\commerce_klarna_checkout;

use Drupal\commerce_klarna_checkout\Event\KlarnaCheckoutEvents;
use Drupal\commerce_klarna_checkout\Event\KlarnaOrderEvent;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Handles payment gateway callbacks.
 */
class CallbackHandler implements CallbackHandlerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The key value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueFactory;

  /**
   * Constructs a new CallbackHandler object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, KeyValueFactoryInterface $key_value_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->keyValueFactory = $key_value_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function processOrderValidation(Request $request) : void {
    $klarna_order = Json::decode($request->getContent());
    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $order_storage->load($klarna_order['merchant_reference2']);

    if (!$order) {
      throw new \RuntimeException(sprintf('Cannot find a matching order for the given Klarna order ID %s.', $klarna_order['order_id']));
    }

    try {
      $event = new KlarnaOrderEvent($klarna_order, $order);
      $this->eventDispatcher->dispatch($event, KlarnaCheckoutEvents::ORDER_VALIDATION);
    }
    catch (\Exception $exception) {
      $key_value_store = $this->keyValueFactory->get('commerce_klarna_checkout.order_validation');
      // Store in the key value a FLAG indicating the Klarna order could not
      // be validated properly.
      $key_value_store->set('order:' . $order->id(), TRUE);
      throw new PaymentGatewayException($exception->getMessage());
    }

    if (!$order->isLocked()) {
      // Lock order and proceed to confirmation.
      $order->lock();
      $order->setRefreshState(Order::REFRESH_SKIP);
      $order->save();
    }
  }

}
