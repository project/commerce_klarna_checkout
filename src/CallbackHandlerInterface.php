<?php

namespace Drupal\commerce_klarna_checkout;

use Symfony\Component\HttpFoundation\Request;

/**
 * Handles callbacks received by Klarna.
 */
interface CallbackHandlerInterface {

  /**
   * Processes the order validation request from Klarna.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The order validation request.
   *
   * @throws \RuntimeException
   *   If the order ID found in the Klarna merchant reference doesn't exist.
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   Thrown by event subscribers when the Klarna order validation fails.
   */
  public function processOrderValidation(Request $request): void;

}
