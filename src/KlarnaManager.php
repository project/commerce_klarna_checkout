<?php

namespace Drupal\commerce_klarna_checkout;

use Drupal\commerce_klarna_checkout\Event\KlarnaCheckoutEvents;
use Drupal\commerce_klarna_checkout\Event\KlarnaOrderEvent;
use Drupal\commerce_klarna_checkout\Event\OrderRequestEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Url;
use Klarna\Rest\Checkout\Order as KlarnaOrder;
use Klarna\Rest\OrderManagement\Order as KlarnaOrderManagement;
use Klarna\Rest\Transport\ConnectorInterface;
use Klarna\Rest\Transport\GuzzleConnector;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Manages Klarna Checkout order.
 */
class KlarnaManager implements KlarnaManagerInterface {

  use LoggerAwareTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The request builder.
   *
   * @var \Drupal\commerce_klarna_checkout\RequestBuilderInterface
   */
  protected $requestBuilder;

  /**
   * The payment gateway plugin configuration.
   *
   * @var array
   */
  protected $config;

  /**
   * The Klarna HTTP transport connector.
   *
   * @var \Klarna\Rest\Transport\ConnectorInterface
   */
  protected $connector;

  /**
   * The minor units converter.
   *
   * @var \Drupal\commerce_price\MinorUnitsConverterInterface
   */
  protected $minorUnitsConverter;

  /**
   * Constructs a new KlarnaManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\commerce_klarna_checkout\RequestBuilderInterface $request_builder
   *   The request builder.
   * @param array $config
   *   The payment gateway plugin configuration array.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, RequestBuilderInterface $request_builder, array $config) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->requestBuilder = $request_builder;
    $this->config = $config;
    $this->initConnector();
  }

  /**
   * Initialize the HTTP transport connector.
   */
  protected function initConnector() {
    // See https://developers.klarna.com/api/#api-urls.
    if ($this->config['purchase_country'] === 'US') {
      $endpoint = $this->config['mode'] === 'test' ? ConnectorInterface::NA_TEST_BASE_URL : ConnectorInterface::NA_BASE_URL;
    }
    else {
      $endpoint = $this->config['mode'] === 'test' ? ConnectorInterface::EU_TEST_BASE_URL : ConnectorInterface::EU_BASE_URL;
    }
    $this->connector = GuzzleConnector::create(
      $this->config['username'],
      $this->config['password'],
      $endpoint
    );
  }

  /**
   * {@inheritdoc}
   */
  public function acknowledgeOrder(OrderInterface $order) {
    $this->assertOrder($order);
    $klarna_order = new KlarnaOrderManagement($this->connector, $order->getData('klarna_order_id'));
    try {
      $klarna_order->acknowledge();
      $this->log(sprintf('Successful Klarna order acknowledgment request for the order %s.', $order->id()));
    }
    catch (\Exception $exception) {
      $this->log(sprintf('Unsuccessful Klarna order acknowledgment request for the order %s (Code: %s).', $order->id(), $exception->getCode()));
      throw $exception;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function acknowledgeKlarnaOrder(string $klarna_order_id, ?OrderInterface $order = NULL) {
    try {
      // Try getting the order from the Klarna checkout endpoint.
      $klarna_order = (array) $this->loadOrder($klarna_order_id);
    }
    catch (\Exception $exception) {
      // Fallback to loading it from the order management API.
      $klarna_order = (array) $this->loadCompletedOrder($klarna_order_id);
    }
    if (!$order) {
      $order_storage = $this->entityTypeManager->getStorage('commerce_order');
      $order = $order_storage->loadForUpdate($klarna_order['merchant_reference2']);
    }
    $event = new KlarnaOrderEvent($klarna_order, $order);
    $this->eventDispatcher->dispatch($event, KlarnaCheckoutEvents::ACKNOWLEDGE_ORDER);
    if (isset($klarna_order['billing_address']['email'])) {
      $order->setEmail($klarna_order['billing_address']['email']);
    }
    // We have to save the order since the billing profile and/or the email were
    // potentially updated.
    $order->setRefreshState(OrderInterface::REFRESH_SKIP);
    $order->save();
    $this->acknowledgeOrder($order);

    return $klarna_order;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelOrder(OrderInterface $order) {
    $this->assertOrder($order);
    $klarna_order = new KlarnaOrderManagement($this->connector, $order->getData('klarna_order_id'));
    try {
      $klarna_order->cancel();
      $this->log(sprintf('Successful Klarna order cancellation request for the order %s.', $order->id()));
    }
    catch (\Exception $exception) {
      $this->log(sprintf('Unsuccessful Klarna order cancellation request for the order %s (Response code: %s).', $order->id(), $exception->getCode()), RfcLogLevel::ERROR);
      throw $exception;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function cancelKlarnaOrder(string $klarna_order_id) {
    $klarna_order = new KlarnaOrderManagement($this->connector, $klarna_order_id);
    try {
      $klarna_order->cancel();
      $this->log(sprintf('Successful Klarna order cancellation request for the klarna order id %s.', $klarna_order_id));
    }
    catch (\Exception $exception) {
      $this->log(sprintf('Unsuccessful Klarna order cancellation request for the klarna order id %s (Response code: %s).', $klarna_order_id, $exception->getCode()), RfcLogLevel::ERROR);
      throw $exception;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createOrder(OrderInterface $order, array $merchant_urls) {
    $request_data = $this->buildOrderRequest($order, $merchant_urls);
    $event = new OrderRequestEvent($order, $request_data);
    $this->eventDispatcher->dispatch($event, KlarnaCheckoutEvents::CREATE_ORDER_REQUEST);
    $klarna_order = new KlarnaOrder($this->connector);
    $request_data = $event->getRequestData();
    $klarna_order = $klarna_order->create($request_data);
    $this->log(sprintf('Creating a Klarna order for the order %s with the following data: <pre>%s</pre>.', $order->id(), print_r($request_data, TRUE)));
    $klarna_order->fetch();
    return $klarna_order;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder(OrderInterface $order) {
    $this->assertOrder($order);
    $klarna_order = new KlarnaOrder($this->connector, $order->getData('klarna_order_id'));
    $klarna_order->fetch();
    return $klarna_order;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOrder($klarna_order_id) {
    $klarna_order = new KlarnaOrder($this->connector, $klarna_order_id);
    $klarna_order->fetch();
    return $klarna_order;
  }

  /**
   * {@inheritdoc}
   */
  public function loadCompletedOrder(string $klarna_order_id) {
    $klarna_order = new KlarnaOrderManagement($this->connector, $klarna_order_id);
    $klarna_order->fetch();
    return $klarna_order;
  }

  /**
   * {@inheritdoc}
   */
  public function updateOrder(OrderInterface $order, array $merchant_urls) {
    $request_data = $this->buildOrderRequest($order, $merchant_urls);
    $event = new OrderRequestEvent($order, $request_data);
    $this->eventDispatcher->dispatch($event, KlarnaCheckoutEvents::UPDATE_ORDER_REQUEST);
    $klarna_order = new KlarnaOrder($this->connector, $order->getData('klarna_order_id'));
    $request_data = $event->getRequestData();
    $this->log(sprintf('Updating the Klarna order for the order %s with the following data: <pre>%s</pre>.', $order->id(), print_r($request_data, TRUE)));
    $klarna_order->update($request_data);
    return $klarna_order;
  }

  /**
   * {@inheritdoc}
   */
  public function createCapture(OrderInterface $order, array $data) {
    $this->assertOrder($order);
    if (!isset($data['captured_amount'])) {
      throw new \InvalidArgumentException("Missing required 'captured_amount' key in the passed data.");
    }
    $data += [
      'order_lines' => $this->requestBuilder->buildOrderLines($order),
    ];
    $event = new OrderRequestEvent($order, $data);
    $this->eventDispatcher->dispatch($event, KlarnaCheckoutEvents::PAYMENT_CAPTURE_REQUEST);
    $data = $event->getRequestData();
    $klarna_order = new KlarnaOrderManagement($this->connector, $order->getData('klarna_order_id'));
    $this->log(sprintf('Klarna payment capture request for the order %s: <pre>%s</pre>.', $order->id(), print_r($data, TRUE)));
    return $klarna_order->createCapture($data);
  }

  /**
   * {@inheritdoc}
   */
  public function createRefund(OrderInterface $order, array $data) {
    $this->assertOrder($order);
    if (!isset($data['refunded_amount'])) {
      throw new \InvalidArgumentException("Missing required 'refunded_amount' key in the passed data.");
    }
    $event = new OrderRequestEvent($order, $data);
    $this->eventDispatcher->dispatch($event, KlarnaCheckoutEvents::PAYMENT_REFUND_REQUEST);
    $data = $event->getRequestData();
    $klarna_order = new KlarnaOrderManagement($this->connector, $order->getData('klarna_order_id'));
    $this->log(sprintf('Klarna payment refund request for the order %s: <pre>%s</pre>.', $order->id(), print_r($data, TRUE)));
    return $klarna_order->refund($data);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAuthorization(OrderInterface $order, array $data = []) {
    $this->assertOrder($order);
    if ($this->minorUnitsConverter) {
      $order_amount = $this->minorUnitsConverter->toMinorUnits($order->getTotalPrice());
    }
    else {
      $order_amount = $this->requestBuilder->toMinorUnits($order->getTotalPrice());
    }
    $data += [
      'order_amount' => $order_amount,
      'order_lines' => $this->requestBuilder->buildOrderLines($order),
    ];
    $event = new OrderRequestEvent($order, $data);
    $this->eventDispatcher->dispatch($event, KlarnaCheckoutEvents::UPDATE_AUTHORIZATION_REQUEST);
    $data = $event->getRequestData();
    $this->log(sprintf('Klarna update authorization request for the order %s with the following data: <pre>%s</pre>.', $order->id(), print_r($data, TRUE)));
    $klarna_order = new KlarnaOrderManagement($this->connector, $order->getData('klarna_order_id'));
    return $klarna_order->updateAuthorization($data);
  }

  /**
   * Sets the minor units converter.
   *
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   */
  public function setMinorUnitsConverter(MinorUnitsConverterInterface $minor_units_converter) {
    $this->minorUnitsConverter = $minor_units_converter;
  }

  /**
   * Builds the order request array for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $merchant_urls
   *   The merchant URLS.
   *
   * @return array
   *   The order request array.
   */
  protected function buildOrderRequest(OrderInterface $order, array $merchant_urls) {
    foreach (['checkout', 'confirmation', 'push'] as $required_key) {
      if (!isset($merchant_urls[$required_key])) {
        throw new \InvalidArgumentException(sprintf('Missing required key %s in the provided $merchant_urls array.', $required_key));
      }
    }
    // Ensure a ?klarna_validation_destination parameter is passed.
    if (isset($merchant_urls['validation'])) {
      $query = parse_url($merchant_urls['validation'], PHP_URL_QUERY);
      parse_str($query, $query_params);
      if (!isset($query_params['klarna_validation_destination'])) {
        throw new \InvalidArgumentException('The "validation" merchant url must contain a ?klarna_validation_destination query parameter.');
      }
    }
    if (is_null($order->getTotalPrice())) {
      throw new \InvalidArgumentException(sprintf('Cannot build the order request with an empty order total for order id %s.', $order->id()));
    }

    $terms_path = $this->config['terms_path'];
    $terms_uri = UrlHelper::isExternal($terms_path) ? Url::fromUri($terms_path) : Url::fromUserInput('/' . ltrim($terms_path, '/'), ['absolute' => TRUE]);
    $data = [
      'purchase_country' => $this->config['purchase_country'],
      'locale' => $this->config['locale'],
      'merchant_urls' => $merchant_urls + [
        'terms' => $terms_uri->toString(),
      ],
      'options' => [
        'allow_separate_shipping_address' => (bool) $this->config['allow_separate_shipping_address'],
      ],
    ];

    if (isset($merchant_urls['validation'])) {
      $data['options']['require_validate_callback_success'] = TRUE;
    }

    if (!empty($this->config['allowed_customer_types'])) {
      $data['options']['allowed_customer_types'] = $this->config['allowed_customer_types'];
    }
    $data += $this->requestBuilder->buildOrder($order);

    return $data;
  }

  /**
   * Asserts that the order holds a reference to the Klarna order ID.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @throws \InvalidArgumentException
   *   Thrown if the order doesn't hold a reference to the Klarna order ID.
   */
  protected function assertOrder(OrderInterface $order) {
    if (!$order->getData('klarna_order_id')) {
      throw new \InvalidArgumentException(sprintf('Missing Klarna order ID for order %s.', $order->id()));
    }
  }

  /**
   * Helper function used for logging API requests.
   *
   * @param string $message
   *   The log message.
   * @param int $level
   *   The log level.
   */
  protected function log(string $message, int $level = RfcLogLevel::INFO): void {
    // If a logger isn't set, this means requests logging isn't enabled.
    if (!isset($this->logger)) {
      return;
    }
    $this->logger->log($level, $message);
  }

}
