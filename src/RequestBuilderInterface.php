<?php

namespace Drupal\commerce_klarna_checkout;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Builds the klarna order and all its components.
 */
interface RequestBuilderInterface {

  /**
   * Builds an address in a format expected by Klarna for the given profile.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile to build an address for.
   *
   * @return array
   *   The formatted address for use in Klarna API calls.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *   If the complex data structure is unset and no item can be created.
   */
  public function buildAddress(ProfileInterface $profile): array;

  /**
   * Builds the order lines for Klarna.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   The order lines as expected by Klarna.
   */
  public function buildOrderLines(OrderInterface $order): array;

  /**
   * Builds the order array for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   The order request array.
   */
  public function buildOrder(OrderInterface $order): array;

  /**
   * Converts the given amount to its minor units.
   *
   * For example, 9.99 USD becomes 999 (Copied from PaymentGatewayBase).
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return int
   *   The amount in minor units, as an integer.
   *
   * @deprecated in commerce_klarna_checkout:8.x-2.1 and is removed from
   *   commerce_klarna_checkout:3.0.0. Use minor units converter.
   * @see https://www.drupal.org/project/commerce_klarna_checkout/issues/3228374
   */
  public function toMinorUnits(Price $amount): int;

}
