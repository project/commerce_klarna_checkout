<?php

namespace Drupal\commerce_klarna_checkout\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;

/**
 * Provides the interface for the Klarna Checkout payment gateway.
 */
interface KlarnaCheckoutInterface extends OffsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface {

  /**
   * Cancels the Klarna order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to cancel.
   */
  public function cancelOrder(OrderInterface $order);

  /**
   * Gets the URL to the "validation" page.
   *
   * When supported, this page is called asynchronously to validate payment
   * before submitting at Klarna.
   *
   * @return \Drupal\Core\Url
   *   The "validation" page url.
   */
  public function getValidationUrl();

  /**
   * Converts an amount in "minor unit" to a decimal amount.
   *
   * For example, 999 USD becomes 9.99.
   *
   * @param mixed $amount
   *   The amount in minor unit.
   * @param string $currency_code
   *   The currency code.
   *
   * @return \Drupal\commerce_price\Price
   *   The decimal price.
   *
   * @deprecated in commerce_klarna_checkout:8.x-2.1 and is removed from
   *   commerce_klarna_checkout:3.0.0. Use minor units converter.
   * @see https://www.drupal.org/project/commerce_klarna_checkout/issues/3228374
   */
  public function fromMinorUnits($amount, $currency_code);

}
