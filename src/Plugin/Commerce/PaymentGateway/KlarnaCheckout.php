<?php

namespace Drupal\commerce_klarna_checkout\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Utility\Error;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Klarna checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "klarna_checkout",
 *   label = "Klarna Checkout",
 *   display_label = "Klarna Checkout",
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_klarna_checkout\PluginForm\OffsiteRedirect\KlarnaCheckoutForm",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class KlarnaCheckout extends OffsitePaymentGatewayBase implements KlarnaCheckoutInterface {

  /**
   * The Klarna manager factory.
   *
   * @var \Drupal\commerce_klarna_checkout\KlarnaManagerFactoryInterface
   */
  protected $klarnaManagerFactory;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The callback handler.
   *
   * @var \Drupal\commerce_klarna_checkout\CallbackHandlerInterface
   */
  protected $callbackHandler;

  /**
   * The Klarna manager.
   *
   * @var \Drupal\commerce_klarna_checkout\KlarnaManagerInterface|null
   */
  protected $klarnaManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->klarnaManagerFactory = $container->get('commerce_klarna_checkout.manager_factory');
    $instance->logger = $container->get('logger.channel.commerce_klarna_checkout');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->callbackHandler = $container->get('commerce_klarna_checkout.callback_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'username' => '',
      'password' => '',
      'capture' => FALSE,
      'purchase_country' => '',
      'locale' => 'sv-se',
      'terms_path' => '',
      'enable_order_validation' => FALSE,
      'update_billing_profile' => TRUE,
      'update_shipping_profile' => FALSE,
      'allowed_customer_types' => [],
      'allow_separate_shipping_address' => FALSE,
      'log_requests' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $this->configuration['username'],
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $this->configuration['password'],
      '#required' => TRUE,
    ];

    $form['capture'] = [
      '#type' => 'radios',
      '#title' => $this->t('Transaction mode'),
      '#options' => [
        TRUE => $this->t('Authorize and capture'),
        FALSE => $this->t('Authorize only (requires manual capture after checkout)'),
      ],
      '#default_value' => (int) $this->configuration['capture'],
    ];

    $form['purchase_country'] = [
      '#type' => 'select',
      '#options' => [
        'AT' => $this->t('Austria'),
        'AU' => $this->t('Australia'),
        'DK' => $this->t('Denmark'),
        'FI' => $this->t('Finland'),
        'DE' => $this->t('Germany'),
        'NL' => $this->t('Netherlands'),
        'NO' => $this->t('Norway'),
        'SE' => $this->t('Sweden'),
        'CH' => $this->t('Switzerland'),
        'GB' => $this->t('United Kingdom'),
        'US' => $this->t('United States'),
      ],
      '#title' => t('Purchase country'),
      '#default_value' => $this->configuration['purchase_country'],
      '#required' => TRUE,
    ];

    $form['locale'] = [
      '#type' => 'select',
      '#title' => $this->t('Locale'),
      '#default_value' => $this->configuration['locale'],
      '#required' => TRUE,
      '#options' => [
        'en-au' => $this->t('English (en-au)'),
        'en-at' => $this->t('English (en-at)'),
        'en-ch' => $this->t('English (en-ch)'),
        'en-de' => $this->t('English (en-de)'),
        'en-dk' => $this->t('English (en-dk)'),
        'en-us' => $this->t('English (en-us)'),
        'en-gb' => $this->t('English (en-gb)'),
        'en-fi' => $this->t('English (en-fi)'),
        'en-nl' => $this->t('English (en-nl)'),
        'en-no' => $this->t('English (en-no)'),
        'da-dk' => $this->t('Danish (da-dk)'),
        'nl-nl' => $this->t('Dutch (nl-nl)'),
        'fr-ch' => $this->t('French (fr-ch)'),
        'fi-fi' => $this->t('Finnish (fi-fi)'),
        'de-de' => $this->t('German (de-de)'),
        'it-ch' => $this->t('Italian (it-ch)'),
        'nb-no' => $this->t('Norwegian (nb-no)'),
        'sv-se' => $this->t('Swedish (sv-se)'),
        'sv-fi' => $this->t('Swedish (sv-fi)'),
      ],
    ];

    $form['terms_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to terms and conditions page'),
      '#default_value' => $this->configuration['terms_path'],
      '#required' => TRUE,
    ];

    $form['enable_order_validation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Validate the order before it is completed'),
      '#default_value' => $this->configuration['enable_order_validation'],
    ];

    $form['update_billing_profile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update the billing customer profile with address information the customer enters at Klarna.'),
      '#default_value' => $this->configuration['update_billing_profile'],
    ];

    $form['update_shipping_profile'] = [
      '#type' => 'checkbox',
      '#title' => t('Update the shipping customer profile (if present) with address information the customer enters at Klarna.'),
      '#default_value' => $this->configuration['update_shipping_profile'],
      '#access' => $this->moduleHandler->moduleExists('commerce_shipping'),
    ];

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Options'),
      '#open' => TRUE,
    ];

    $form['options']['log_requests'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log API requests'),
      '#default_value' => $this->configuration['log_requests'],
      '#parents' => array_merge($form['#parents'], ['log_requests']),
    ];

    $form['options']['allowed_customer_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed customer types'),
      '#options' => [
        'person' => $this->t('Person'),
        'organization' => $this->t('Organization'),
      ],
      '#default_value' => $this->configuration['allowed_customer_types'],
      '#parents' => array_merge($form['#parents'], ['allowed_customer_types']),
    ];

    $form['options']['allow_separate_shipping_address'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow separate shipping address'),
      '#description' => $this->t('If true, the consumer can enter different billing and shipping addresses.'),
      '#default_value' => $this->configuration['allow_separate_shipping_address'],
      '#parents' => array_merge($form['#parents'], ['allow_separate_shipping_address']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if ($form_state->getErrors()) {
      return;
    }
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['username'] = $values['username'];
    $this->configuration['password'] = $values['password'];
    $this->configuration['capture'] = !empty($values['capture']);
    $this->configuration['purchase_country'] = $values['purchase_country'];
    $this->configuration['locale'] = $values['locale'];
    $this->configuration['terms_path'] = $values['terms_path'];
    $this->configuration['enable_order_validation'] = !empty($values['enable_order_validation']);
    $this->configuration['update_billing_profile'] = $values['update_billing_profile'];
    $customer_types = array_values(array_filter($values['allowed_customer_types']));
    $this->configuration['update_shipping_profile'] = !empty($values['update_shipping_profile']);

    if ($customer_types) {
      $this->configuration['allowed_customer_types'] = $customer_types;
    }
    $this->configuration['allow_separate_shipping_address'] = $values['allow_separate_shipping_address'];
    $this->configuration['log_requests'] = !empty($values['log_requests']);
  }

  /**
   * {@inheritdoc}
   */
  public function getNotifyUrl() {
    $notify_url = parent::getNotifyUrl();
    $notify_url->setOption('query', [
      'klarna_order_id' => '{checkout.order.id}',
    ]);
    return $notify_url;
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    switch ($request->query->get('callback')) {
      case 'validation':
        try {
          $this->callbackHandler->processOrderValidation($request);

          return new Response('', Response::HTTP_OK);
        }
        catch (\Exception $exception) {
          $this->logger->error($exception->getMessage());
          return $this->buildFailedValidationResponse($request);
        }
      default:
        $klarna_order_id = $request->query->get('klarna_order_id');
        if (empty($klarna_order_id)) {
          $this->logger->error('Cannot acknowledge the Klarna order: no order ID provided by Klarna.');
          return NULL;
        }
        try {
          $this->acknowledgeOrder($klarna_order_id);
        }
        catch (\Exception $exception) {
          $this->logger->error($exception->getMessage());
          return NULL;
        }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $klarna_order_id = $order->getData('klarna_order_id');
    if (!$klarna_order_id) {
      throw new PaymentGatewayException(sprintf('Cannot acknowledge the Klarna order (Unknown Klarna ID for order ID %s.', $order->id()));
    }
    // According to the Klarna API documentation, there should be an attempt to
    // acknowledge the order from the confirmation callback.
    $this->acknowledgeOrder($klarna_order_id, $order);
  }

  /**
   * Acknowledges remote order.
   *
   * Acknowledge the given Klarna order, and optionally capture the payment if
   * configured to do so.
   *
   * @param string $klarna_order_id
   *   The Klarna order id.
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $order
   *   (optional) The order, fetched from the Klarna merchant reference if not
   *   provided.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function acknowledgeOrder($klarna_order_id, ?OrderInterface $order = NULL) {
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->loadByRemoteId($klarna_order_id);

    // If a payment already exists in the system for the given order, it means
    // the order was already acknowledged.
    if ($payment) {
      return;
    }
    try {
      $klarna_order = $this->getKlarnaManager()->acknowledgeKlarnaOrder($klarna_order_id, $order);
    }
    catch (\Exception $exception) {
      // Exception is thrown so Klarna can retry until order is acknowledged.
      // Note: Klarna will send the push notifications every four hours for a
      // total of 48 hours or until you confirm that you have received the
      // order.
      // @see https://developers.klarna.com/documentation/klarna-checkout/integration-guide/confirm-purchase/
      throw new PaymentGatewayException($exception->getMessage());
    }
    $payment_amount = $this->minorUnitsConverter->fromMinorUnits($klarna_order['order_amount'], $klarna_order['purchase_currency']);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->create([
      'state' => 'authorization',
      // Use the amount we get from Klarna, rather than the order total, since
      // it might be different.
      'amount' => $payment_amount,
      'payment_gateway' => isset($this->parentEntity) ? $this->parentEntity->id() : $this->entityId,
      'order_id' => $klarna_order['merchant_reference2'],
      'test' => $this->getMode() === 'test',
      'remote_id' => $klarna_order['order_id'],
      'remote_state' => $klarna_order['status'],
    ]);
    // Capture the payment right away, if configured to do so.
    if (!empty($this->configuration['capture'])) {
      $this->capturePayment($payment);
    }
    else {
      $payment->save();
    }
  }

  /**
   * Builds a response for when the Klarna validation fails.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  protected function buildFailedValidationResponse(Request $request) {
    $response = new TrustedRedirectResponse($request->query->get('klarna_validation_destination'), 303);
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheMaxAge(0);
    $response->addCacheableDependency($cacheable_metadata);

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $order = $payment->getOrder();

    try {
      $parameters = [
        'captured_amount' => $this->minorUnitsConverter->toMinorUnits($amount),
      ];
      $this->getKlarnaManager()->createCapture($order, $parameters);
      $payment->setState('completed');
      $payment->setAmount($amount);
      $payment->save();
    }
    catch (\Exception $exception) {
      // When an exception occurs, check if the order is already captured.
      // This can happen in case a previous capture was successful but for
      // some reason the request was considered as unsuccessful, due to a
      // network error for example.
      try {
        $klarna_order = $this->getKlarnaManager()->loadCompletedOrder($order->getData('klarna_order_id'));

        // The order is fully captured, mark the Drupal payment as "completed".
        if ($klarna_order['status'] === 'CAPTURED') {
          $captured_amount = $this->minorUnitsConverter->fromMinorUnits($klarna_order['captured_amount'], $klarna_order['purchase_currency']);
          $payment->setState('completed');
          $payment->setAmount($captured_amount);
          $payment->save();
          return;
        }
      }
      catch (\Exception $another_exception) {
        // We log the new exception, but we don't throw a payment gateway
        // exception as the original exception feels more relevant here.
        $this->logger->error($another_exception->getMessage());
      }

      throw new PaymentGatewayException($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);

    try {
      $this->getKlarnaManager()->cancelKlarnaOrder($payment->getRemoteId());
      $payment->setState('voided');
      $payment->save();
    }
    catch (\Exception $exception) {
      throw new PaymentGatewayException($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);
    $order = $payment->getOrder();

    try {
      $parameters = [
        'refunded_amount' => $this->minorUnitsConverter->toMinorUnits($amount),
      ];
      $this->getKlarnaManager()->createRefund($order, $parameters);
    }
    catch (\Exception $exception) {
      throw new PaymentGatewayException($exception->getMessage());
    }
    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function cancelOrder(OrderInterface $order) {
    try {
      /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      // Void all the referenced payments after cancellation.
      $payments = $payment_storage->loadMultipleByOrder($order);

      foreach ($payments as $payment) {
        $this->getKlarnaManager()->cancelKlarnaOrder($payment->getRemoteId());
        $payment->getState()->applyTransitionById('void');
        $payment->save();
      }
    }
    catch (\Exception $exception) {
      Error::logException($this->logger, $exception);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValidationUrl() {
    return Url::fromRoute('commerce_payment.notify', [
      'commerce_payment_gateway' => $this->parentEntity->id(),
    ], [
      'absolute' => TRUE,
      'query' => [
        'klarna_order_id' => '{checkout.order.id}',
        'callback' => 'validation',
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function fromMinorUnits($amount, $currency_code) {
    return $this->minorUnitsConverter->fromMinorUnits($amount, $currency_code);
  }

  /**
   * Returns an instantiated Klarna manager.
   *
   * @return \Drupal\commerce_klarna_checkout\KlarnaManagerInterface
   *   An instantiated Klarna manager.
   */
  protected function getKlarnaManager() {
    if (isset($this->klarnaManager)) {
      return $this->klarnaManager;
    }
    $klarna_manager = $this->klarnaManagerFactory->get($this->getConfiguration());
    if (!empty($this->configuration['log_requests'])) {
      $klarna_manager->setLogger($this->logger);
    }
    $this->klarnaManager = $klarna_manager;
    return $this->klarnaManager;
  }

}
