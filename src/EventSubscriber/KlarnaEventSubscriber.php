<?php

namespace Drupal\commerce_klarna_checkout\EventSubscriber;

use Drupal\commerce_klarna_checkout\Event\KlarnaCheckoutEvents;
use Drupal\commerce_klarna_checkout\Event\KlarnaOrderEvent;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\profile\Entity\ProfileInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Reacts to Klarna events.
 *
 * @see https://www.drupal.org/project/commerce_klarna_checkout/issues/3150349
 */
class KlarnaEventSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The minor units converter.
   *
   * @var \Drupal\commerce_price\MinorUnitsConverterInterface
   */
  protected $minorUnitsConverter;

  /**
   * Constructs a new KlarnaEventSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MinorUnitsConverterInterface $minor_units_converter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->minorUnitsConverter = $minor_units_converter;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KlarnaCheckoutEvents::ACKNOWLEDGE_ORDER => ['onAcknowledge'],
      KlarnaCheckoutEvents::ORDER_VALIDATION => ['onValidate'],
    ];
  }

  /**
   * Populates the billing/shipping profile on order acknowledgment.
   *
   * @param \Drupal\commerce_klarna_checkout\Event\KlarnaOrderEvent $event
   *   The Klarna order event.
   */
  public function onAcknowledge(KlarnaOrderEvent $event) {
    $order = $event->getOrder();
    $klarna_order = $event->getKlarnaOrder();
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->entity;
    $configuration = $payment_gateway->getPluginConfiguration();

    // Update the billing profile if configured to do so.
    if (!empty($configuration['update_billing_profile']) && isset($klarna_order['billing_address'])) {
      $profile = $order->getBillingProfile();
      if (!$profile) {
        $profile = $this->entityTypeManager->getStorage('profile')->create([
          'uid' => 0,
          'type' => 'customer',
        ]);
        $order->setBillingProfile($profile);
      }
      $this->populateProfile($profile, $klarna_order['billing_address']);
      $profile->save();
    }

    // Update the shipping profile if configured to do so.
    if (!empty($configuration['update_shipping_profile']) && isset($klarna_order['shipping_address'])) {
      $profiles = $order->collectProfiles();

      if (isset($profiles['shipping'])) {
        $this->populateProfile($profiles['shipping'], $klarna_order['shipping_address']);
        $profiles['shipping']->save();
      }
    }
  }

  /**
   * Ensures the Klarna order total matches ours.
   *
   * @param \Drupal\commerce_klarna_checkout\Event\KlarnaOrderEvent $event
   *   The Klarna order event.
   */
  public function onValidate(KlarnaOrderEvent $event) {
    $klarna_order = $event->getKlarnaOrder();
    $order = $event->getOrder();
    $klarna_total = $this->minorUnitsConverter->fromMinorUnits($klarna_order['order_amount'], $klarna_order['purchase_currency']);

    if (!$klarna_total->equals($order->getTotalPrice())) {
      throw new PaymentGatewayException(sprintf('Order total mismatch for the following Klarna order (Klarna ID: %s, order ID: %s)', $klarna_order['order_id'], $order->id()));
    }
  }

  /**
   * Populates the given profile with the given Klarna address.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile to populate.
   * @param array $address
   *   The Klarna address.
   */
  protected function populateProfile(ProfileInterface $profile, array $address) {
    $mapping = [
      'organization_name' => 'organization',
      'street_address' => 'address_line1',
      'street_address2' => 'address_line2',
      'city' => 'locality',
      'region' => 'administrative_area',
      'postal_code' => 'postal_code',
      'country' => 'country_code',
      'given_name' => 'given_name',
      'family_name' => 'family_name',
    ];
    foreach ($address as $key => $value) {
      if (!isset($mapping[$key])) {
        continue;
      }
      $value = $key === 'country' ? strtoupper($value) : $value;
      $profile->address->{$mapping[$key]} = $value;
    }
  }

}
