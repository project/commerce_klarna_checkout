<?php

namespace Drupal\commerce_klarna_checkout\EventSubscriber;

use Drupal\commerce_klarna_checkout\Plugin\Commerce\PaymentGateway\KlarnaCheckoutInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Cancels the Klarna order when the order is canceled.
 */
class OrderSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      'commerce_order.cancel.post_transition' => ['onCancel'],
    ];
  }

  /**
   * Cancels the Klarna order when the order is canceled.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onCancel(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    if (!$this->isPaidWithKlarna($order)) {
      return;
    }
    /** @var \Drupal\commerce_klarna_checkout\Plugin\Commerce\PaymentGateway\KlarnaCheckoutInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $order->get('payment_gateway')->entity->getPlugin();
    $payment_gateway_plugin->cancelOrder($order);
  }

  /**
   * Returns whether the order's payment gateway is Klarna.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return bool
   *   TRUE if the order's payment gateway is Klarna, FALSE otherwise.
   */
  protected function isPaidWithKlarna(OrderInterface $order) {
    $payment_gateway = !$order->get('payment_gateway')->isEmpty() ? $order->get('payment_gateway')->entity : FALSE;
    return $payment_gateway instanceof PaymentGatewayInterface && $payment_gateway->getPlugin() instanceof KlarnaCheckoutInterface;
  }

}
