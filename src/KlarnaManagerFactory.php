<?php

namespace Drupal\commerce_klarna_checkout;

use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines a factory for the Klarna manager.
 */
class KlarnaManagerFactory implements KlarnaManagerFactoryInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The request builder.
   *
   * @var \Drupal\commerce_klarna_checkout\RequestBuilderInterface
   */
  protected $requestBuilder;

  /**
   * The minor units converter.
   *
   * @var \Drupal\commerce_price\MinorUnitsConverterInterface
   */
  protected $minorUnitsConverter;

  /**
   * Array of all instantiated Klarna managers.
   *
   * @var \Drupal\commerce_klarna_checkout\KlarnaManagerInterface[]
   */
  protected $instances = [];

  /**
   * Constructs a new KlarnaManagerFactory object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\commerce_klarna_checkout\RequestBuilderInterface $request_builder
   *   The request builder.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, RequestBuilderInterface $request_builder, MinorUnitsConverterInterface $minor_units_converter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->requestBuilder = $request_builder;
    $this->minorUnitsConverter = $minor_units_converter;
  }

  /**
   * {@inheritdoc}
   */
  public function get(array $configuration) {
    $username = $configuration['username'];
    if (!isset($this->instances[$username])) {
      $klarna_manager = new KlarnaManager($this->entityTypeManager, $this->eventDispatcher, $this->requestBuilder, $configuration);
      $klarna_manager->setMinorUnitsConverter($this->minorUnitsConverter);
      $this->instances[$username] = $klarna_manager;
    }

    return $this->instances[$username];
  }

}
