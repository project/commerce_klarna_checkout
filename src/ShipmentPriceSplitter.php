<?php

namespace Drupal\commerce_klarna_checkout;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Splits the shipment amounts across tax rates.
 */
class ShipmentPriceSplitter implements ShipmentPriceSplitterInterface {

  /**
   * The currency storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $currencyStorage;

  /**
   * The rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * Constructs a new ShipmentPriceSplitter object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   The rounder.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RounderInterface $rounder) {
    $this->currencyStorage = $entity_type_manager->getStorage('commerce_currency');
    $this->rounder = $rounder;
  }

  /**
   * {@inheritdoc}
   */
  public function split(OrderInterface $order, ShipmentInterface $shipment) {
    // Group order items by tax percentage.
    $groups = [];
    foreach ($order->getItems() as $order_item) {
      $order_item_total = $order_item->getTotalPrice();
      $order_item_tax_adjustments = $order_item->getAdjustments(['tax']);
      if ($order_item_tax_adjustments) {
        $order_item_tax_adjustment = reset($order_item_tax_adjustments);
        $percentage = $order_item_tax_adjustment->getPercentage();
      }
      else {
        $percentage = '0';
      }
      if (!isset($groups[$percentage])) {
        $groups[$percentage] = $order_item_total;
      }
      else {
        $previous_total = $groups[$percentage];
        $groups[$percentage] = $previous_total->add($order_item_total);
      }
    }
    // Sort by percentage descending.
    krsort($groups, SORT_NUMERIC);
    // Calculate the ratio of each group.
    $subtotal = $order->getSubtotalPrice()->getNumber();
    $ratios = [];
    foreach ($groups as $percentage => $order_item_total) {
      // Cannot calculate the ratio if the order item total is 0 for this tax
      // percentage group.
      if ($order_item_total->isZero()) {
        $ratios[$percentage] = '0';
        continue;
      }
      $ratios[$percentage] = $order_item_total->divide($subtotal)->getNumber();
    }
    // We need to send the "unit_price" to Klarna (that includes tax and
    // excludes discount).
    // That is what the "original_amount" is supposed to be designed for except
    // that doesn't take into account the "altered" amounts.
    $unit_price = $shipment->getAmount();
    // Check if we have promotion adjustments.
    $promotion_adjustments = $shipment->getAdjustments(['shipping_promotion']);
    $promotion_total = new Price('0', $shipment->getAmount()->getCurrencyCode());
    /** @var \Drupal\commerce_order\Adjustment $adjustment */
    foreach ($promotion_adjustments as $adjustment) {
      $promotion_amount = $adjustment->getAmount()->multiply(-1);
      $promotion_total = $promotion_total->add($promotion_amount);
      // Add any included promotion adjustment to the unit price.
      if ($adjustment->isIncluded()) {
        $unit_price = $unit_price->add($promotion_amount);
      }
    }
    $promotion_amounts = [];
    if (!$promotion_total->isZero()) {
      $promotion_amounts = $this->allocate($promotion_total, $ratios);
    }

    $return = [];
    // The unit price should not include any discount.
    $unit_amounts = $this->allocate($unit_price, $ratios);
    $adjusted_shipment_amounts = $this->allocate($shipment->getAdjustedAmount(['shipping_promotion']), $ratios);
    $shipment_tax_adjustments = $shipment->getAdjustments(['tax']);
    // Re-key the tax adjustments by tax percentage for easier retrieval.
    $keyed_tax_adjustments = [];
    // Because there can be tax exempt items in the order affecting the ratios,
    // we need to recalculate the tax amounts, otherwise Klarna would complain
    // that the tax amount doesn't match the tax rate passed.
    foreach ($shipment_tax_adjustments as $adjustment) {
      $percentage = $adjustment->getPercentage();
      if (!isset($adjusted_shipment_amounts[$percentage])) {
        continue;
      }
      $tax_amount = $this->calculateTaxAmount($adjusted_shipment_amounts[$percentage], $percentage, $adjustment->isIncluded());
      $tax_amount = $this->rounder->round($tax_amount);
      $keyed_tax_adjustments[$percentage] = $tax_amount;
    }
    $zero_price = new Price('0', $shipment->getAmount()->getCurrencyCode());
    foreach ($ratios as $percentage => $ratio) {
      $return[$percentage] = [
        'promotions_amount' => $promotion_amounts[$percentage] ?? $zero_price,
        'unit_amount' => $unit_amounts[$percentage] ?? $zero_price,
        'adjusted_shipment_amount' => $adjusted_shipment_amounts[$percentage] ?? $zero_price,
        'tax_amount' => $keyed_tax_adjustments[$percentage] ?? $zero_price,
        'tax_rate' => $percentage,
      ];
    }

    return $return;
  }

  /**
   * Allocates the given amount according to a list of ratios.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   * @param array $ratios
   *   An array of ratios, keyed by tax percentage.
   *
   * @return array
   *   An array of amounts keyed by tax percentage.
   */
  protected function allocate(Price $amount, array $ratios) {
    $amounts = [];
    $amount_to_allocate = $amount;
    foreach ($ratios as $percentage => $ratio) {
      $individual_amount = $amount_to_allocate->multiply($ratio);
      $individual_amount = $this->rounder->round($individual_amount, PHP_ROUND_HALF_DOWN);
      // Due to rounding it is possible for the last calculated
      // per-order-item amount to be larger than the total remaining amount.
      if ($individual_amount->greaterThan($amount)) {
        $individual_amount = $amount;
      }
      $amounts[$percentage] = $individual_amount;
      $amount = $amount->subtract($individual_amount);
    }
    // The individual amounts don't add up to the full amount, distribute
    // the reminder among them.
    if (!$amount->isZero()) {
      /** @var \Drupal\commerce_price\Entity\CurrencyInterface $currency */
      $currency = $this->currencyStorage->load($amount->getCurrencyCode());
      $precision = $currency->getFractionDigits();
      // Use the smallest rounded currency amount (e.g. '0.01' for USD).
      $smallest_number = Calculator::divide('1', pow(10, $precision), $precision);
      $smallest_amount = new Price($smallest_number, $amount->getCurrencyCode());
      while (!$amount->isZero()) {
        foreach ($amounts as $percentage => $individual_amount) {
          $amounts[$percentage] = $individual_amount->add($smallest_amount);
          $amount = $amount->subtract($smallest_amount);
          if ($amount->isZero()) {
            break 2;
          }
        }
      }
    }

    return $amounts;
  }

  /**
   * Calculates the tax amount for the given shipping amount.
   *
   * This is copied from the commerce_shipping module.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The proportional shipping amount.
   * @param string $percentage
   *   The tax rate percentage.
   * @param bool $included
   *   Whether tax is already included in the price.
   *
   * @return \Drupal\commerce_price\Price
   *   The unrounded tax amount.
   */
  protected function calculateTaxAmount(Price $amount, $percentage, $included = FALSE) {
    $tax_amount = $amount->multiply($percentage);
    if ($included) {
      $divisor = Calculator::add('1', $percentage);
      $tax_amount = $tax_amount->divide($divisor);
    }

    return $tax_amount;
  }

}
