<?php

namespace Drupal\commerce_klarna_checkout;

use Drupal\commerce_order\Entity\OrderInterface;
use Psr\Log\LoggerAwareInterface;

/**
 * Manages Klarna Checkout order.
 */
interface KlarnaManagerInterface extends LoggerAwareInterface {

  /**
   * Acknowledges the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @see https://developers.klarna.com/api/#order-management-api-acknowledge-order
   *
   * @throws \InvalidArgumentException
   *   If the order doesn't hold a reference to the Klarna order ID.
   */
  public function acknowledgeOrder(OrderInterface $order);

  /**
   * Acknowledges the given Klarna order.
   *
   * @param string $klarna_order_id
   *   The Klarna order id.
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $order
   *   (optional) The order, fetched from the Klarna merchant reference if not
   *   provided.
   *
   * @return array
   *   The Klarna order.
   *
   * @see https://developers.klarna.com/api/#order-management-api-acknowledge-order
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function acknowledgeKlarnaOrder(string $klarna_order_id, ?OrderInterface $order = NULL);

  /**
   * Cancels the Klarna checkout order for the given order.
   *
   * This is called by voidPayment().
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @throws \InvalidArgumentException
   *   If the order doesn't hold a reference to the Klarna order ID.
   */
  public function cancelOrder(OrderInterface $order);

  /**
   * Cancels the Klarna checkout order for the given Klarna order ID.
   *
   * @param string $klarna_order_id
   *   The Klarna order id.
   */
  public function cancelKlarnaOrder(string $klarna_order_id);

  /**
   * Creates a new Klarna checkout order for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $merchant_urls
   *   An associative array containing at least the following keys:
   *   - 'checkout': The url to the checkout page.
   *   - 'confirmation': The url of the checkout confirmation page.
   *   - 'push': URL that will be requested when an order is completed.
   *   Note that if a 'validation' url is passed, it must contain a
   *   ?klarna_validation_destination query parameter.
   *
   * @see https://developers.klarna.com/api/#checkout-api-create-an-order
   *
   * @return \Klarna\Rest\Checkout\Order
   *   The Klarna checkout order.
   *
   * @throws \InvalidArgumentException
   *   If the provided $merchant_urls array is incomplete.
   */
  public function createOrder(OrderInterface $order, array $merchant_urls);

  /**
   * Gets the Klarna checkout order for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Klarna\Rest\Checkout\Order
   *   The Klarna checkout order.
   *
   * @throws \InvalidArgumentException
   *   If the order doesn't hold a reference to the Klarna order ID.
   */
  public function getOrder(OrderInterface $order);

  /**
   * Loads a specific Klarna order.
   *
   * @param string $klarna_order_id
   *   The Klarna order ID.
   *
   * @return \Klarna\Rest\Checkout\Order
   *   The Klarna checkout order.
   */
  public function loadOrder($klarna_order_id);

  /**
   * Loads an order from the Klarna order management API.
   *
   * Once the customer has completed the purchase, the order should be loaded
   * from this endpoint.
   *
   * @param string $klarna_order_id
   *   The Klarna order ID.
   *
   * @return \Klarna\Rest\OrderManagement\Order
   *   The Klarna order (retrieved from the order management API).
   */
  public function loadCompletedOrder(string $klarna_order_id);

  /**
   * Updates the order in Klarna.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $merchant_urls
   *   An associative array containing at least the following keys:
   *   - 'checkout': The url to the checkout page.
   *   - 'confirmation': The url of the checkout confirmation page.
   *   - 'push': URL that will be requested when an order is completed.
   *   Note that if a 'validation' url is passed, it must contain a
   *   ?klarna_validation_destination query parameter.
   *
   * @see https://developers.klarna.com/api/#checkout-api-create-an-order
   *
   * @return \Klarna\Rest\Checkout\Order
   *   The Klarna checkout order.
   *
   * @throws \InvalidArgumentException
   *   If the provided $merchant_urls array is incomplete.
   */
  public function updateOrder(OrderInterface $order, array $merchant_urls);

  /**
   * Creates a capture for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $data
   *   A data array containing at least the "captured_amount" key,
   *   in minor units.
   *
   * @return \Klarna\Rest\OrderManagement\Capture
   *   The capture resource.
   *
   * @throws \InvalidArgumentException
   *   If the provided $data array is incomplete.
   */
  public function createCapture(OrderInterface $order, array $data);

  /**
   * Creates a refund.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $data
   *   A data array containing at least the "refunded_amount" key,
   *   in minor units.
   *
   * @return \Klarna\Rest\OrderManagement\Refund
   *   The created refund resource.
   *
   * @throws \InvalidArgumentException
   *   If the provided $data array is incomplete.
   */
  public function createRefund(OrderInterface $order, array $data);

  /**
   * Updates the authorization data.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $data
   *   (optional) A data array.
   *   See https://developers.klarna.com/api/#order-management-api-set-new-order-amount-and-order-lines.
   *
   * @throws \InvalidArgumentException
   *   If the order doesn't hold a reference to the Klarna order ID.
   */
  public function updateAuthorization(OrderInterface $order, array $data = []);

}
