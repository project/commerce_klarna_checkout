<?php

namespace Drupal\commerce_klarna_checkout\PluginForm\OffsiteRedirect;

use Drupal\commerce_klarna_checkout\KlarnaManagerFactoryInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Offsite payment gateway form.
 *
 * This form displays markup snippet received from Klarna API.
 */
class KlarnaCheckoutForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The Klarna manager factory.
   *
   * @var \Drupal\commerce_klarna_checkout\KlarnaManagerFactoryInterface
   */
  protected $klarnaManagerFactory;

  /**
   * The key-value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValueStore;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new KlarnaCheckoutForm object.
   *
   * @param \Drupal\commerce_klarna_checkout\KlarnaManagerFactoryInterface $klarna_manager_factory
   *   The Klarna manager factory.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(KlarnaManagerFactoryInterface $klarna_manager_factory, KeyValueFactoryInterface $key_value_factory, MessengerInterface $messenger) {
    $this->klarnaManagerFactory = $klarna_manager_factory;
    $this->keyValueStore = $key_value_factory->get('commerce_klarna_checkout.order_validation');
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_klarna_checkout.manager_factory'),
      $container->get('keyvalue'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    /** @var \Drupal\commerce_klarna_checkout\Plugin\Commerce\PaymentGateway\KlarnaCheckoutInterface $plugin */
    $plugin = $this->plugin;
    $configuration = $plugin->getConfiguration();
    $klarna_manager = $this->klarnaManagerFactory->get($configuration);
    // Check if we already have a Klarna order ID for this order.
    $merchant_urls = [
      'checkout' => $form['#cancel_url'],
      'confirmation' => $form['#return_url'],
      // We need to decode the notify url because of {checkout.order.id}.
      'push' => urldecode($plugin->getNotifyUrl()->toString()),
    ];
    if (!empty($configuration['enable_order_validation'])) {
      $validation_url = $plugin->getValidationUrl();
      $validation_url->mergeOptions(['query' => ['klarna_validation_destination' => Url::fromRoute('<current>')->toString()]]);
      $merchant_urls['validation'] = urldecode($validation_url->toString());
    }

    if ($order->getData('klarna_order_id')) {
      try {
        $klarna_order = $klarna_manager->updateOrder($order, $merchant_urls);
      }
      catch (\Exception $exception) {
        // The Klarna order ID might be invalid, proceed to creating a new one.
        $klarna_order = NULL;
      }
    }

    if (!isset($klarna_order)) {
      try {
        $klarna_order = $klarna_manager->createOrder($order, $merchant_urls);
        $order->setData('klarna_order_id', $klarna_order->getId());
        $order->setRefreshState(Order::REFRESH_SKIP);
        $order->save();
      }
      catch (\Exception $exception) {
        throw new PaymentGatewayException($exception->getMessage());
      }
    }

    // Display validation error if one set.
    if ($this->keyValueStore->get('order:' . $order->id())) {
      $this->messenger->addError($this->t('Payment failed at the payment server. Please review your information and try again.'));
      $this->keyValueStore->delete('order:' . $order->id());
    }

    $form['klarna'] = [
      '#markup' => Markup::create($klarna_order['html_snippet']),
    ];

    return $form;
  }

}
