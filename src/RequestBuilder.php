<?php

namespace Drupal\commerce_klarna_checkout;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\commerce_order\AdjustmentTransformerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * This helper service builds the klarna order and all its components.
 */
class RequestBuilder implements RequestBuilderInterface {

  /**
   * The adjustment transformer.
   *
   * @var \Drupal\commerce_order\AdjustmentTransformerInterface
   */
  protected $adjustmentTransformer;

  /**
   * The country repository.
   *
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected $countryRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The minor units converter.
   *
   * @var \Drupal\commerce_price\MinorUnitsConverterInterface
   */
  protected $minorUnitsConverter;

  /**
   * Constructs a new RequestBuilder object.
   *
   * @param \Drupal\commerce_order\AdjustmentTransformerInterface $adjustment_transformer
   *   The adjustment transformer.
   * @param \CommerceGuys\Addressing\Country\CountryRepositoryInterface $country_repository
   *   The country repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   */
  public function __construct(AdjustmentTransformerInterface $adjustment_transformer, CountryRepositoryInterface $country_repository, EntityTypeManagerInterface $entity_type_manager, MinorUnitsConverterInterface $minor_units_converter) {
    $this->adjustmentTransformer = $adjustment_transformer;
    $this->countryRepository = $country_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->minorUnitsConverter = $minor_units_converter;
  }

  /**
   * {@inheritdoc}
   */
  public function buildAddress(ProfileInterface $profile): array {
    /** @var \Drupal\address\AddressInterface $address */
    $address = $profile->get('address')->first();
    return [
      'organization_name' => $address->getOrganization(),
      'given_name' => $address->getGivenName(),
      'family_name' => $address->getFamilyName(),
      'country' => $address->getCountryCode(),
      'postal_code' => $address->getPostalCode(),
      'city' => $address->getLocality(),
      'region' => $address->getAdministrativeArea(),
      'street_address' => $address->getAddressLine1(),
      'street_address2' => $address->getAddressLine2(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOrder(OrderInterface $order): array {
    $data = [];
    $store = $order->getStore();
    $data['purchase_currency'] = $order->getTotalPrice()->getCurrencyCode();
    $data['name'] = $store->label();
    $data['order_lines'] = $this->buildOrderLines($order);
    $data['order_amount'] = $this->minorUnitsConverter->toMinorUnits($order->getTotalPrice());
    $data['merchant_reference1'] = $order->getOrderNumber() ?: $order->id();
    $data['merchant_reference2'] = $order->id();

    // Set allowed countries.
    $country_list = $this->countryRepository->getList();
    $data['billing_countries'] = array_column($store->get('billing_countries')->getValue(), 'value') ?: array_keys($country_list);
    if ($store->hasField('shipping_countries')) {
      $data['shipping_countries'] = array_column($store->get('shipping_countries')->getValue(), 'value') ?: array_keys($country_list);
    }

    $profiles = $order->collectProfiles();
    // Send the billing profile only if not null.
    if (isset($profiles['billing'])) {
      $billing_address = $this->buildAddress($profiles['billing']);
      $data['billing_address'] = $billing_address;
    }
    if ($order->getEmail()) {
      $data['billing_address']['email'] = $order->getEmail();
    }

    // Calculate the order tax amount.
    // Note that we don't simply sum the order tax adjustments as there can
    // be some complex proportional tax distribution logic occurring with
    // shipments that may affect the tax amounts sent (in case the order
    // contains tax exempt order items, mainly).
    $order_tax_amount = 0;
    foreach ($data['order_lines'] as $order_line) {
      if (empty($order_line['total_tax_amount'])) {
        continue;
      }
      $order_tax_amount += $order_line['total_tax_amount'];
    }
    $data['order_tax_amount'] = $order_tax_amount;

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOrderLines(OrderInterface $order): array {
    $order_lines = [];
    foreach ($order->getItems() as $order_item) {
      // Skip malformed order items.
      if (!$order_item->getUnitPrice()) {
        continue;
      }
      // Fallback to the order item ID.
      $reference = $order_item->id();
      $purchased_entity = $order_item->getPurchasedEntity();

      // Send the "SKU" as the "reference" for product variations.
      if ($purchased_entity instanceof ProductVariationInterface) {
        $reference = $purchased_entity->getSku();
      }
      $name = trim($order_item->label());
      $order_line = [
        // The reference is limited to 64 characters.
        'reference' => mb_substr($reference, 0, 64),
        'name' => !empty($name) ? $name : $reference,
        'quantity' => (int) $order_item->getQuantity(),
        'unit_price' => $this->minorUnitsConverter->toMinorUnits($order_item->getUnitPrice()),
        'total_amount' => $this->minorUnitsConverter->toMinorUnits($order_item->getAdjustedTotalPrice(['promotion'])),
        'tax_rate' => 0,
        'total_tax_amount' => 0,
      ];

      // Only pass included tax adjustments (i.e "VAT"), non included tax
      // adjustments are passed separately (i.e "Sales tax").
      $tax_adjustments = $order_item->getAdjustments(['tax']);
      if ($tax_adjustments && $tax_adjustments[0]->isIncluded()) {
        $tax_rate = $tax_adjustments[0]->getPercentage();
        $order_line = array_merge($order_line, [
          'tax_rate' => (int) Calculator::multiply($tax_rate, '10000'),
          'total_tax_amount' => $this->minorUnitsConverter->toMinorUnits($tax_adjustments[0]->getAmount()),
        ]);
      }

      // Check if we have promotion adjustments.
      $promotion_adjustments = $order_item->getAdjustments(['promotion']);
      if ($promotion_adjustments  && ($promotions_total = $this->getAdjustmentsTotal($promotion_adjustments, [], TRUE))) {
        $order_line['total_discount_amount'] = $this->minorUnitsConverter->toMinorUnits($promotions_total->multiply('-1'));
      }
      $order_lines[] = $order_line;
    }
    $adjustment_types_mapping = [
      'tax' => 'sales_tax',
      'fee' => 'surcharge',
    ];
    // Shipping is handled separately.
    $exclude_adjustment_types = ['shipping', 'promotion', 'shipping_promotion'];
    $adjustments = $order->collectAdjustments();
    $adjustments = $adjustments ? $this->adjustmentTransformer->processAdjustments($adjustments) : [];
    foreach ($adjustments as $adjustment) {
      $adjustment_type = $adjustment->getType();
      // Skip included adjustments and the ones we don't handle.
      if ($adjustment->isIncluded() ||
        in_array($adjustment_type, $exclude_adjustment_types)) {
        continue;
      }
      $order_line = [
        // The reference is limited to 64 characters.
        'reference' => $adjustment->getSourceId() ? mb_substr($adjustment->getSourceId(), 0, 64) : '',
        'name' => $adjustment->getLabel(),
        'quantity' => 1,
        'tax_rate' => 0,
        'total_tax_amount' => 0,
        'unit_price' => $this->minorUnitsConverter->toMinorUnits($adjustment->getAmount()),
        'total_amount' => $this->minorUnitsConverter->toMinorUnits($adjustment->getAmount()),
      ];

      // Map the adjustment type to the type expected by Klarna.
      if (isset($adjustment_types_mapping[$adjustment_type])) {
        $order_line['type'] = $adjustment_types_mapping[$adjustment_type];
      }

      $order_lines[] = $order_line;
    }
    if ($order->hasField('shipments') && !$order->get('shipments')->isEmpty()) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments */
      $shipments = $order->get('shipments')->referencedEntities();
      foreach ($shipments as $shipment) {
        $shipment_amount = $shipment->getAmount();
        if (empty($shipment_amount)) {
          continue;
        }
        // Check if there are included tax adjustments.
        $tax_adjustments = array_filter($shipment->getAdjustments(['tax']), function ($adjustment) {
          // Skip "0" tax adjustments, there's no need to distribute the
          // shipment amount in this case.
          return $adjustment->isIncluded() && !$adjustment->getAmount()->isZero();
        });
        $shipping_line = [
          'name' => (string) t('Shipping'),
          'quantity' => 1,
          'type' => 'shipping_fee',
        ];
        // If there are no included tax adjustments, there's no need to split
        // the shipment amounts across multiple VAT rates.
        if (!$tax_adjustments) {
          $shipping_line += [
            'total_amount' => $this->minorUnitsConverter->toMinorUnits($shipment->getAdjustedAmount(['shipping_promotion'])),
            'tax_rate' => 0,
            'total_tax_amount' => 0,
          ];
          // The unit price shouldn't include discounts, we cannot use the
          // original amount as the "unit_price" because it won't reflect the
          // correct price if shipping rates were altered.
          $unit_price = $shipment->getAmount();
          $promotion_adjustments = $shipment->getAdjustments(['shipping_promotion']);
          if ($promotion_adjustments && ($promotions_total = $this->getAdjustmentsTotal($promotion_adjustments, [], TRUE))) {
            $shipping_line['total_discount_amount'] = $this->minorUnitsConverter->toMinorUnits($promotions_total->multiply('-1'));
          }
          $shipping_line['unit_price'] = $this->minorUnitsConverter->toMinorUnits($unit_price);
          $order_lines[] = $shipping_line;
        }
        else {
          /** @var \Drupal\commerce_klarna_checkout\ShipmentPriceSplitterInterface $shipment_price_splitter */
          $shipment_price_splitter = \Drupal::service('commerce_klarna_checkout.shipment_price_splitter');
          $shipment_amounts = $shipment_price_splitter->split($order, $shipment);
          /** @var \Drupal\commerce_price\Price[] $amounts */
          foreach ($shipment_amounts as $amounts) {
            $order_lines[] = $shipping_line + [
              'unit_price' => $this->minorUnitsConverter->toMinorUnits($amounts['unit_amount']),
              'total_amount' => $this->minorUnitsConverter->toMinorUnits($amounts['adjusted_shipment_amount']),
              'tax_rate' => $amounts['tax_amount']->isZero() ? 0 : (int) Calculator::multiply($amounts['tax_rate'], '10000'),
              'total_tax_amount' => $this->minorUnitsConverter->toMinorUnits($amounts['tax_amount']),
              'total_discount_amount' => $this->minorUnitsConverter->toMinorUnits($amounts['promotions_amount']),
            ];
          }
        }
      }
    }

    return $order_lines;
  }

  /**
   * Calculates the total for the given adjustments.
   *
   * @param \Drupal\commerce_order\Adjustment[] $adjustments
   *   The adjustments.
   * @param string[] $adjustment_types
   *   The adjustment types to include in the calculation.
   *   Examples: fee, promotion, tax. Defaults to all adjustment types.
   * @param bool $skip_included
   *   Whether to skip included adjustments (Defaults to FALSE).
   *
   * @return \Drupal\commerce_price\Price|null
   *   The adjustments total, or NULL if no matching adjustments were found.
   */
  protected function getAdjustmentsTotal(array $adjustments, array $adjustment_types = [], $skip_included = FALSE) {
    $adjustments_total = NULL;
    $matching_adjustments = [];

    foreach ($adjustments as $adjustment) {
      if ($skip_included && $adjustment->isIncluded()) {
        continue;
      }
      if ($adjustment_types && !in_array($adjustment->getType(), $adjustment_types)) {
        continue;
      }
      $matching_adjustments[] = $adjustment;
    }
    if ($matching_adjustments) {
      $matching_adjustments = $this->adjustmentTransformer->processAdjustments($matching_adjustments);
      foreach ($matching_adjustments as $adjustment) {
        $adjustments_total = $adjustments_total ? $adjustments_total->add($adjustment->getAmount()) : $adjustment->getAmount();
      }
    }

    return $adjustments_total;
  }

  /**
   * {@inheritdoc}
   */
  public function toMinorUnits(Price $amount): int {
    return $this->minorUnitsConverter->toMinorUnits($amount);
  }

}
