<?php

namespace Drupal\commerce_klarna_checkout\Event;

use Drupal\commerce\EventBase;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Represents a Klarna order event.
 *
 * @see \Drupal\commerce_klarna_checkout\Event\KlarnaCheckoutEvents
 */
class KlarnaOrderEvent extends EventBase {

  /**
   * The Klarna order.
   *
   * @var array
   */
  protected $klarnaOrder;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Constructs a new KlarnaOrderEvent object.
   *
   * @param array $klarna_order
   *   The Klarna order.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function __construct(array $klarna_order, OrderInterface $order) {
    $this->klarnaOrder = $klarna_order;
    $this->order = $order;
  }

  /**
   * Gets the Klarna order.
   *
   * @return array
   *   The Klarna order.
   */
  public function getKlarnaOrder(): array {
    return $this->klarnaOrder;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order entity.
   */
  public function getOrder() : OrderInterface {
    return $this->order;
  }

}
