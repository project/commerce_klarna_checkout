<?php

namespace Drupal\commerce_klarna_checkout\Event;

/**
 * Defines events for the Commerce Klarna Checkout module.
 */
final class KlarnaCheckoutEvents {

  /**
   * Name of the event fired when calling Klarna for creating an order.
   *
   * @Event
   *
   * @see \Drupal\commerce_klarna_checkout\Event\OrderRequestEvent
   */
  const CREATE_ORDER_REQUEST = 'commerce_klarna_checkout.create_order_request';

  /**
   * Name of the event fired when calling Klarna for updating an order.
   *
   * @Event
   *
   * @see \Drupal\commerce_klarna_checkout\Event\OrderRequestEvent
   */
  const UPDATE_ORDER_REQUEST = 'commerce_klarna_checkout.update_order_request';

  /**
   * Name of the event fired when Klarna sending callback validation request.
   *
   * @Event
   *
   * @see \Drupal\commerce_klarna_checkout\Event\KlarnaOrderEvent
   */
  const ORDER_VALIDATION = 'commerce_klarna_checkout.order_validation';

  /**
   * Name of the event fired when Klarna order is acknowledged.
   *
   * @Event
   *
   * @see \Drupal\commerce_klarna_checkout\Event\KlarnaOrderEvent
   */
  const ACKNOWLEDGE_ORDER = 'commerce_klarna_checkout.acknowledge_order';

  /**
   * Name of the event fired when the authorization is updated.
   *
   * @Event
   *
   * @see \Drupal\commerce_klarna_checkout\Event\OrderRequestEvent
   */
  const UPDATE_AUTHORIZATION_REQUEST = 'commerce_klarna_checkout.update_authorization_request';

  /**
   * Name of the event fired before the payment capture request is made.
   *
   * @Event
   *
   * @see \Drupal\commerce_klarna_checkout\Event\OrderRequestEvent
   */
  const PAYMENT_CAPTURE_REQUEST = 'commerce_klarna_checkout.payment_capture_request';

  /**
   * Name of the event fired before the payment refund request is made.
   *
   * @Event
   *
   * @see \Drupal\commerce_klarna_checkout\Event\OrderRequestEvent
   */
  const PAYMENT_REFUND_REQUEST = 'commerce_klarna_checkout.payment_refund_request';

}
